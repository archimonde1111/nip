// Copyright Epic Games, Inc. All Rights Reserved.

#include "NIP.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NIP, "NIP" );

#define ECC_Interactive ECC_GameTraceChannel1 