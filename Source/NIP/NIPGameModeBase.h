// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NIPGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class NIP_API ANIPGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
