// Miki&Dawid


#include "Components/CameraShakeClass.h"
#include "Math/UnrealMathUtility.h"

UCameraShakeClass::UCameraShakeClass()
{
  OscillationDuration = 999999.f;
  OscillationBlendInTime = 0.4f;
  OscillationBlendOutTime = 0.4f;
}