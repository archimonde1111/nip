// Miki&Dawid


#include "Components/CharacterAbilities.h"


UCharacterAbilities::UCharacterAbilities()
{
	
	PrimaryComponentTick.bCanEverTick = true;

    CurrentStaminaCharacter = 100.f;

}



void UCharacterAbilities::BeginPlay()
{
	Super::BeginPlay();

	
	
}



void UCharacterAbilities::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UCharacterAbilities::GetCurrentStamina()
{
	return CurrentStaminaCharacter;           
}

void UCharacterAbilities::ChangeStamina(float amountOfStamina)
{
    CurrentStaminaCharacter = CurrentStaminaCharacter + amountOfStamina; 
    if(CurrentStaminaCharacter > 100)
	{
		CurrentStaminaCharacter = 100.f;    
	}

	if(CurrentStaminaCharacter < 0)
	{
		CurrentStaminaCharacter = 0.f;
	}
}

