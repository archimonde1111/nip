// Miki&Dawid


#include "Components/FlashlightActorComponent.h"
#include "Components/SpotLightComponent.h"
#include "Pawns/Characters/PlayerCharacter.h"
#include "Math/UnrealMathUtility.h"

// Sets default values for this component's properties
UFlashlightActorComponent::UFlashlightActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
    
	Flashlight = CreateDefaultSubobject<USpotLightComponent>(TEXT("Flashlight"));
	Flashlight->OuterConeAngle = 25.f;
	Flashlight->AttenuationRadius = 2000.f;
	Flashlight->SetIntensity(0.f);
	flashlightStatus = false;
	BatteryCurrentValue = 100.f;
}



void UFlashlightActorComponent::BeginPlay()
{
	Super::BeginPlay();
}



void UFlashlightActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	ChangeBatteryMinusPlus(DeltaTime);
	SetFlashlightIntensity();
}

void UFlashlightActorComponent::ToggleFlashlight()
{
  if(flashlightStatus==true)
  {
	flashlightStatus = false;
  }else{
	flashlightStatus = true;
  }
} 

void UFlashlightActorComponent::ChangeBattery(float batteryChangeValue)
{
	BatteryCurrentValue = BatteryCurrentValue + batteryChangeValue; 
    
	BatteryCurrentValue = FMath::Clamp(BatteryCurrentValue, 0.0f, 100.0f);
}

void UFlashlightActorComponent::ChangeBatteryMinusPlus(float DeltaTime)
{
	if(flashlightStatus==true&&BatteryCurrentValue>=0.f)
	{
        ChangeBattery(DeltaTime * 100.f * (-0.20f));
	}else if(flashlightStatus==false&&BatteryCurrentValue<=100.f){
        ChangeBattery(DeltaTime * 100.f * (0.20f));
	}
}

float UFlashlightActorComponent::GetCurrentBattery()
{
	return BatteryCurrentValue;
}

void UFlashlightActorComponent::SetFlashlightIntensity()
{
	if(flashlightStatus==true&&BatteryCurrentValue>0.f)
	{
		Flashlight->SetIntensity(FlashlightIntensity);
	}else{
		Flashlight->SetIntensity(0.f);
	}
}

