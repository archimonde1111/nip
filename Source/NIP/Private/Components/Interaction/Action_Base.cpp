// Miki&Dawid


#pragma once

#include "Components/Interaction/Action_Base.h"


void UAction_Base::BeginAction_Implementation()
{
    bActionStarted = true;
}
void UAction_Base::TickAction_Implementation()
{

}
void UAction_Base::EndAction_Implementation()
{
    bActionStarted = false;
}

void UAction_Base::Interact(AActor* inInteractor)
{
    if(!inInteractor) { return; }
    Interactor = inInteractor;

    BeginAction();
}
