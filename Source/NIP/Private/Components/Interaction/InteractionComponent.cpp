// Miki&Dawid


#include "Components/Interaction/InteractionComponent.h"
#include "Components/ShapeComponent.h"
#include "Components/StaticMeshComponent.h"

#include "Components/Interaction/Action_Base.h"
#include "Pawns/Characters/ControllableCharacter.h"


UInteractionComponent::UInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}



void UInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	if(AvailableAction)
		AvailableAction->SetOwner(GetOwner());
}


void UInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(AvailableAction && AvailableAction->CanActionTick())
		AvailableAction->TickAction();
}

bool UInteractionComponent::CanInteract(AActor* otherActor)
{
	if(!IsValid(AvailableAction))
	{
		UE_LOG(LogTemp, Warning, TEXT("Action wasn't set in %s in InteractionComponent"), *GetOwner()->GetName());
		return false;
	}
	if(!IsValid(otherActor) || !ClassThatCanInteract || !otherActor->GetClass()->IsChildOf(ClassThatCanInteract)) 
	{ 
		return false; 
	}

	return true;
}


void UInteractionComponent::Interact(AActor* interactor)
{
	if(!CanInteract(interactor)) { return; }

	AvailableAction->Interact(interactor); 
}

void UInteractionComponent::HighlightInteractable(bool bShouldHighlight)
{
	TArray<UActorComponent*> OwnersStaticMeshes = {};
	GetOwner()->GetComponents(UStaticMeshComponent::StaticClass(), OwnersStaticMeshes);

	for (UActorComponent* OwnersStaticMeshe : OwnersStaticMeshes)
	{
		UStaticMeshComponent* StaticMesh = Cast<UStaticMeshComponent>(OwnersStaticMeshe);

		if(StaticMesh)
		{
			StaticMesh->SetRenderCustomDepth(bShouldHighlight);
		}
	}
}
