// Miki&Dawid


#include "Components/Interaction/InteractionManagerComponent.h"
#include "Engine/World.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameFramework/Actor.h"

#include "DrawDebugHelpers.h"

#include "Components/Interaction/InteractionComponent.h"


UInteractionManagerComponent::UInteractionManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UInteractionManagerComponent::BeginPlay()
{
	Super::BeginPlay();
}


void UInteractionManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(bUseMultiLineTrace) 
	{ 
		HandleLineTraceUsage(); 
	}

	if(bHighlighFirstInteractable)
	{
		if(AvailableInteractions.IsValidIndex(0))
		{
			HighlightInteractable(AvailableInteractions[0], true);
		}
	}
}
void UInteractionManagerComponent::HandleLineTraceUsage()
{
	TArray<FHitResult> HitResults; 

	//#TODO: Think if this can be more effective(Clearing array every time before LineTrace might not be needed)
	//Clears array so highlighted interactables will extinguish
	ClearArrays();
	SendMultiLineTrace(HitResults);
	SortHitResults(HitResults);
}
void UInteractionManagerComponent::ClearArrays()
{
	for (UInteractionComponent* InteractionComponent : AvailableInteractions)
	{
		if (InteractionComponent)
		{
			InteractionComponent->HighlightInteractable(false);
		}
	}
	AvailableInteractions.Empty();
}

void UInteractionManagerComponent::SendMultiLineTrace(TArray<FHitResult>& outHitResult)
{
	FVector CameraWorldLocation;
	FVector CameraWorldDirection;
	
	bool CameraPositionAcquired = GetCameraWorldLocationAndDirection(CameraWorldLocation, CameraWorldDirection);
	if(!CameraPositionAcquired) 
	{ 
		return; 
	}


	FVector OwnerLocation = GetOwner()->GetActorLocation();
	FVector InteractionRange = CameraWorldDirection * ActorInteractionRange;
	FVector OwnerInteractionMaximumRange = InteractionRange + OwnerLocation;

	FCollisionQueryParams QueryParams = FCollisionQueryParams::DefaultQueryParam;
	QueryParams.AddIgnoredActor(GetOwner());

	bool bBlockHitOccurred = GetWorld()->LineTraceMultiByChannel
	(outHitResult, CameraWorldLocation, OwnerInteractionMaximumRange, ECollisionChannel::ECC_GameTraceChannel1, QueryParams);

	if(bDebugMultiLineTrace)
	{
		DrawDebugLine(GetWorld(), CameraWorldLocation, 				  OwnerInteractionMaximumRange, FColor::Red, false, -1.f, 0, 3.f);

	}


	if((bBlockHitOccurred && outHitResult.Num() == 1) || outHitResult.Num() == 0) 
	{ 
		return; 
	}
	return;
}
bool UInteractionManagerComponent::GetCameraWorldLocationAndDirection(FVector& cameraWorldLocation, FVector& cameraWorldDirection)
{
	APawn* OwningPawn = Cast<APawn> (GetOwner());
	if(!OwningPawn) 
	{ 
		return false; 
	}
	APlayerController* PlayerController = Cast<APlayerController> (OwningPawn->GetController()); 
	if(!PlayerController) 
	{ 
		return false; 
	}
	
	FVector2D ViewportCrosshairPosition = GetViewportCrosshairPosition(PlayerController);
	return PlayerController->DeprojectScreenPositionToWorld(ViewportCrosshairPosition.X, ViewportCrosshairPosition.Y, cameraWorldLocation, cameraWorldDirection);
}
FVector2D UInteractionManagerComponent::GetViewportCrosshairPosition(APlayerController* playerController)
{
	if(!playerController)
	{
		UE_LOG(LogTemp, Error, TEXT("Fail nullptr passed(playerController), UInteractionManagerComponent::GetViewportCrosshairPosition"));
		return FVector2D(0.f);
	}
	int32 ViewportSizeX = 0;
	int32 ViewportSizeY = 0;
	playerController->GetViewportSize(ViewportSizeX, ViewportSizeY);

	FVector2D ViewportCrosshairPosition;
	ViewportCrosshairPosition.X = ViewportSizeX * CrosshairPositionX;
	ViewportCrosshairPosition.Y = ViewportSizeY * CrosshairPositionY;

	return ViewportCrosshairPosition;
}

void UInteractionManagerComponent::SortHitResults(const TArray<FHitResult>& outHitResult)
{
	for(FHitResult HitResult : outHitResult)
	{
		AActor* HittedActor = HitResult.GetActor();
		if(!IsValid(HittedActor))
		{
			continue;
		}

		UInteractionComponent* HittedActorInteractionComponent = Cast<UInteractionComponent> (HittedActor->GetComponentByClass(UInteractionComponent::StaticClass()));
		if(!HittedActorInteractionComponent) 
		{ 
			continue;
		}

		AvailableInteractions.AddUnique(HittedActorInteractionComponent);
	}
}


void UInteractionManagerComponent::PerformInteraction(AActor* interactor)
{
	if(!interactor)
	{ 
		return; 
	}
	if(!AvailableInteractions.IsValidIndex(0)) 
	{ 
		return; 
	}

	AvailableInteractions[0]->Interact(interactor);
	AvailableInteractions[0]->HighlightInteractable(false);
	AActor* InteractionComponentOwner = AvailableInteractions[0]->GetOwner();

	AvailableInteractions.RemoveAt(0);
}


void UInteractionManagerComponent::HighlightInteractable(UInteractionComponent* interactionComponentToHighlight, bool bHighlightInteractable)
{
	if (!interactionComponentToHighlight)
	{
		UE_LOG(LogTemp, Warning, TEXT("Highlighting Interactable filed(interactionComponentToHighlight is null), UInteractionManagerComponent::HighlightInteractable"));
		return;
	}

	interactionComponentToHighlight->HighlightInteractable(bHighlightInteractable);
}
