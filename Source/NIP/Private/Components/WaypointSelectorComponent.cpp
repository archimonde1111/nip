// Miki&Dawid


#include "Components/WaypointSelectorComponent.h"
#include "Pawns/Characters/AIControl/WaypointContainer.h"
#include "Containers/Map.h"
#include "Engine/EngineTypes.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"


UWaypointSelectorComponent::UWaypointSelectorComponent()
{
	
	PrimaryComponentTick.bCanEverTick = true;

	
}



void UWaypointSelectorComponent::BeginPlay()
{
	Super::BeginPlay();

	// #TODO: Change this to something more efficent
	TArray<AActor*> FoundManager;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypointContainer::StaticClass(), FoundManager);
	WaypointContainer = Cast<AWaypointContainer>(FoundManager[0]);

	
}

void UWaypointSelectorComponent::SelectWaypoint()
{
   FTimerHandle TimerHandle;
   if(bActorWaiting == false && WaypointContainer.IsValid())
	{
		bActorWaiting = true;

		if(RouteChange!=RouteNumber)
		{
			ArrayIterator = 0;
		}else{
			ArrayIterator++;
		}
		
			RouteChange = RouteNumber;
			int32 RouteSelectorMaxSize = WaypointContainer->GetLocationAndWaitTimeArray(RouteNumber).Num();
			

		if(RouteSelectorMaxSize<=ArrayIterator)
		{
			ArrayIterator = 0;
		}
		TimeWait = WaypointContainer->GenerateWaypoint(RouteNumber, ArrayIterator).WaitTime;

		if(RouteStatus == ERouteStatus::ESC_Random)
		{
			RandomNumber = FMath::RandRange(0, (RouteSelectorMaxSize-1));
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UWaypointSelectorComponent::SetWaypoint, WaypointContainer->GenerateWaypoint(RouteNumber, RandomNumber).WaitTime, false);
		}else{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UWaypointSelectorComponent::SetWaypoint, TimeWait, false);
		}
	}
}

void UWaypointSelectorComponent::SetWaypoint()
{
	if(RouteStatus == ERouteStatus::ESC_Random)
	{
		SelectedWaypoint = WaypointContainer->GenerateWaypoint(RouteNumber, RandomNumber).TargetWaypoints;
	}else{
		SelectedWaypoint = WaypointContainer->GenerateWaypoint(RouteNumber, ArrayIterator).TargetWaypoints;
	}
	bActorWaiting = false;
}



void UWaypointSelectorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
}

