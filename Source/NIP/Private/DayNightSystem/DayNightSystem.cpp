// Miki&Dawid


#include "DayNightSystem/DayNightSystem.h"
#include "Engine/DirectionalLight.h"
#include "Misc/OutputDeviceNull.h"

// Sets default values
ADayNightSystem::ADayNightSystem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADayNightSystem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADayNightSystem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	LightSourceRotator(DeltaTime);

}

void ADayNightSystem::LightSourceRotator(float DeltaTime)
{ 
if(EnableCycle == true)
{
	if(LightSource)
	{
		LightSource->AddActorLocalRotation(FRotator((DeltaTime * TurnRate), 0, 0));
	}
	if(Sun)
	{
		FOutputDeviceNull argument;
		Sun->CallFunctionByNameWithArguments(TEXT("UpdateSunDirection"),argument, NULL, true);
	}
}
}

