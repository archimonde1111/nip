// Miki&Dawid


#include "HUD/NIPInGameHUD.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "HUD/Widgets/UserWidgetClass.h"

void ANIPInGameHUD::BeginPlay()
{

   Super::BeginPlay();
   if(Widget)
   {
     MyWidget = CreateWidget<UUserWidgetClass>(GetWorld(), Widget);

     if(MyWidget != nullptr)
     {
        MyWidget->AddToViewport();
     }
   }
}