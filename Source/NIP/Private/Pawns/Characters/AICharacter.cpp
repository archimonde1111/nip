// Miki&Dawid


#include "Pawns/Characters/AICharacter.h"
#include "GameFramework/CharacterMovementComponent.h"



AAICharacter::AAICharacter()
{
 
}

void AAICharacter::BeginPlay()
{
	Super::BeginPlay();
  GetCharacterMovement()-> bOrientRotationToMovement = true;
  GetCharacterMovement()-> RotationRate = FRotator(0.0f, 600.0f, 0.0f);
  GetCharacterMovement()->MaxWalkSpeed=330.f;
  

}

