// Miki&Dawid


#include "Pawns/Characters/AIControl/AIControllerBase.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Pawns/Characters/AIControl/WaypointContainer.h"
#include "Components/WaypointSelectorComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

AAIControllerBase::AAIControllerBase()
{
   WaypointSelectorComponent = CreateDefaultSubobject<UWaypointSelectorComponent>("WaypointSelectorComponent");
}


void AAIControllerBase::BeginPlay()
{
    Super::BeginPlay();
	WaypointSelectorComponent->SelectWaypoint();
}

void AAIControllerBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);	
}

void AAIControllerBase::MoveAIToLocation()
{

}

void AAIControllerBase::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
}

FRotator AAIControllerBase::GetControlRotation() const
{
	if (GetPawn() == nullptr)
	{
		return FRotator(0.0f, 0.0f, 0.0f);
	}

	return FRotator(0.0f, GetPawn()->GetActorRotation().Yaw, 0.0f);
	
}