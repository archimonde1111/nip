// Miki&Dawid


#include "Pawns/Characters/AIControl/AIEnemyControllerClass.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Pawns/Characters/AICharacter.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Pawns/Characters/AIControl/WaypointContainer.h"
#include "Pawns/Characters/AIControl/AIControllerBase.h"
#include "Components/WaypointSelectorComponent.h"
#include "Pawns/Characters/ControllableCharacter.h"


AAIEnemyControllerClass::AAIEnemyControllerClass()
{
	SetInConstructor();
}

void AAIEnemyControllerClass::SetInConstructor()
{
    SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

    SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
	GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &AAIEnemyControllerClass::OnPawnDetected);
	GetPerceptionComponent()->ConfigureSense(*SightConfig);
}

void AAIEnemyControllerClass::BeginPlay()
{
    Super::BeginPlay();
    PlayerCharacter =  Cast<AControllableCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void AAIEnemyControllerClass::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);	
	MoveAIToLocation();
}

void AAIEnemyControllerClass::MoveAIToLocation()
{
   Super::MoveAIToLocation();


	if (DistanceToPlayer > AISightRadius)
	      {
		     bIsPlayerDetected = false;
	      }

	if (bIsPlayerDetected == false)
	      {
		     MoveToLocation(WaypointSelectorComponent->SelectedWaypoint, 5.0f);
       	  }
	else if (bIsPlayerDetected == true)
	      {
			if(PlayerCharacter)
			{
				 MoveToActor(PlayerCharacter, 5.0f);
			}
	      }

	if(this->GetMoveStatus() == EPathFollowingStatus::Idle && bIsPlayerDetected == false)
	      {
		    WaypointSelectorComponent->SelectWaypoint();
     	  }
}

void AAIEnemyControllerClass::OnPawnDetected(const TArray<AActor*> &DetectedPawns)
{
    for (size_t i = 0; i < DetectedPawns.Num(); i++)
	{
		if(PlayerCharacter){
			if(DetectedPawns[i]==PlayerCharacter)
		      {
			    DistanceToPlayer = GetPawn()->GetDistanceTo(DetectedPawns[i]);
		         PawnDetectBool = true;
		      }
		}
	}

    if( PawnDetectBool == true)
	{
	bIsPlayerDetected = true;
	}
}
