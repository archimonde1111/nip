// Miki&Dawid


#include "Pawns/Characters/AIControl/WaypointContainer.h"
#include "Containers/Map.h"
#include "Engine/EngineTypes.h"


AWaypointContainer::AWaypointContainer()
{
 	
	PrimaryActorTick.bCanEverTick = true;

}


void AWaypointContainer::BeginPlay()
{
	Super::BeginPlay();
	
}

FLocationAndWaitTime AWaypointContainer::GenerateWaypoint(int32 RouteIndex, int32 WaypointIndex)
{
	return RouteSelection[RouteIndex].LocationAndTime[WaypointIndex];
}


TArray<FLocationAndWaitTime> AWaypointContainer::GetLocationAndWaitTimeArray(int32 RouteIndex)
{
	return RouteSelection[RouteIndex].LocationAndTime;
}



void AWaypointContainer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

