// Miki&Dawid


#include "Pawns/Characters/ControllableCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Components/CapsuleComponent.h"
#include "Math/UnrealMathUtility.h"

#include "Components/Interaction/InteractionManagerComponent.h"


AControllableCharacter::AControllableCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->MaxWalkSpeed=420.f;
	baseTurnRate = 45.0f;
	baseLookUpAtRate = 45.0f;
	SpeedMultiplayer = 2.0f;

}

void AControllableCharacter::BeginPlay()
{
	Super::BeginPlay();

	StatusCharacter = EStatusCharacter::ESC_Walk;
	
	CreateInteractionManager();
}
void AControllableCharacter::CreateInteractionManager()//Temporary method until UE5 will have fixed spawning of default components, for now it has to stay like that...
{
	InteractionManager = NewObject<UInteractionManagerComponent> (this, FName("InteractionManager"));
	InteractionManager->RegisterComponent();
	InteractionManager->bUseMultiLineTrace = true;
	InteractionManager->ActorInteractionRange = 200.f;
}

void AControllableCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AControllableCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	BindMovementInputs(*PlayerInputComponent);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Look", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AControllableCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AControllableCharacter::LookUpAtRate); 

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AControllableCharacter::Interact);
}

void AControllableCharacter::BindMovementInputs(UInputComponent& PlayerInputComponent)
{
    PlayerInputComponent.BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent.BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent.BindAction("Crouch", IE_Pressed, this, &AControllableCharacter::CrouchStart);
	PlayerInputComponent.BindAction("Crouch", IE_Released, this, &AControllableCharacter::CrouchStop);
	PlayerInputComponent.BindAction("Sprint", IE_Pressed, this,  &AControllableCharacter::Sprint);
	PlayerInputComponent.BindAction("Sprint", IE_Released, this, &AControllableCharacter::SprintStop);

	PlayerInputComponent.BindAxis("MoveForward", this, &AControllableCharacter::MoveForward);
	PlayerInputComponent.BindAxis("MoveRight", this, &AControllableCharacter::MoveRight);
}
void AControllableCharacter::CrouchStart()
{
	StatusCharacter = EStatusCharacter::ESC_Crouch;
	Crouch();
}
void AControllableCharacter::CrouchStop()
{
	StatusCharacter = EStatusCharacter::ESC_Walk;
	UnCrouch();
}
void AControllableCharacter::Sprint()
{
  GetCharacterMovement()->MaxWalkSpeed=700.f;
}
void AControllableCharacter::SprintStop()
{
  GetCharacterMovement()->MaxWalkSpeed=420.f;
}
void AControllableCharacter::MoveForward(float value)
{
    if((Controller) && (value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, value);
	}
}
void AControllableCharacter::MoveRight(float value)
{
	if((Controller) && (value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, value);
	}
}

void AControllableCharacter::TurnAtRate(float value)
{
	AddControllerYawInput(value * baseTurnRate * GetWorld()->GetDeltaSeconds());
}
void AControllableCharacter::LookUpAtRate(float value)
{
	AddControllerPitchInput(value * baseLookUpAtRate * GetWorld()->GetDeltaSeconds());
}

void AControllableCharacter::Interact()
{
	if(InteractionManager) 
		InteractionManager->PerformInteraction(this);
}
