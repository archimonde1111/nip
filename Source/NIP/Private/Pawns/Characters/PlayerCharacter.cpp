// Miki&Dawid


#include "Pawns/Characters/PlayerCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/InputComponent.h"
#include "Components/CharacterAbilities.h"
#include "Camera/CameraShake.h"
#include "Components/CameraShakeClass.h"
#include "Components/SpotLightComponent.h"
#include "Components/FlashlightActorComponent.h"



APlayerCharacter::APlayerCharacter()
{
    SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->SetupAttachment(RootComponent);
    
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);

	CharacterAbilitiesPointer = CreateDefaultSubobject<UCharacterAbilities>("CharacterAbilities");
	GetMovementComponent()->NavAgentProps.bCanCrouch = true;

	FlashlightInCharacter = CreateDefaultSubobject<UFlashlightActorComponent>("FlashlightInCharacterTest");
	FlashlightInCharacter->Flashlight->SetupAttachment(CameraComp);

	GetCharacterMovement()->MaxWalkSpeed=420.f;

}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SprintTestStamina(DeltaTime);
	MoveForwardTestMethod();
}

void APlayerCharacter::BeginPlay()
{
	
	Super::BeginPlay();

}

void APlayerCharacter::SprintTestStamina(float DeltaTime)
{
    if(CharacterAbilitiesPointer!=nullptr)
	{
		if (StatusCharacter == EStatusCharacter::ESC_Sprint && CharacterAbilitiesPointer->GetCurrentStamina() > 0) 
		{
			GetCharacterMovement()->MaxWalkSpeed = 700.f;
			CharacterAbilitiesPointer->ChangeStamina(DeltaTime * 100.f * (-0.20f));
		}
		else
		{
			GetCharacterMovement()->MaxWalkSpeed = 420.f;
			CharacterAbilitiesPointer->ChangeStamina(DeltaTime * 100.f * (0.20f));
		}
	}
}

void APlayerCharacter::CameraShakeWalk(float scale)
{
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->StartCameraShake(CamShakeWalk, scale);
}

void APlayerCharacter::CameraShakeSprint(float scale)
{
	GetWorld()->GetFirstPlayerController()->PlayerCameraManager->StartCameraShake(CamShakeSprint, scale);
}

void APlayerCharacter::MoveForwardTestMethod()
{
	if(GetCharacterMovement()->Velocity.Size()>0.5f)
	{
      if(StatusCharacter == EStatusCharacter::ESC_Sprint)
	  {
        CameraShakeSprint(0.06f);
	  }else
	  {
         CameraShakeWalk(0.02f);
	  }
	}else{
	   GetWorld()->GetFirstPlayerController()->PlayerCameraManager->StopAllCameraShakes(true);
		
	}
}

void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if(PlayerInputComponent)
	{
	  PlayerInputComponent->BindAction("Sprint", IE_Pressed, this,  &APlayerCharacter::Sprint);
	  PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::SprintStop);
	  PlayerInputComponent->BindAction("Flashlight", IE_Pressed, this,  &APlayerCharacter::FlashlightSystem);
	}
}

void APlayerCharacter::FlashlightSystem()
{
	FlashlightInCharacter->ToggleFlashlight();
}

void APlayerCharacter::Sprint()
{
  StatusCharacter = EStatusCharacter::ESC_Sprint; 
  CameraComp->SetFieldOfView(82.f);
}

void APlayerCharacter::SprintStop()
{
  CameraComp->SetFieldOfView(90.f);
  StatusCharacter = EStatusCharacter::ESC_Walk; 
  GetWorld()->GetFirstPlayerController()->PlayerCameraManager->StopAllCameraShakes(true);
}
