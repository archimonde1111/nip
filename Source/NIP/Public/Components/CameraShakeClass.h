// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "MatineeCameraShake.h"
#include "CameraShakeClass.generated.h"

/**
 * 
 */
UCLASS()
class NIP_API UCameraShakeClass : public UMatineeCameraShake
{
	GENERATED_BODY()
	
public:
	UCameraShakeClass();
	
};
