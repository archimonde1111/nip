// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterAbilities.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NIP_API UCharacterAbilities : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UCharacterAbilities();

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "CurrentStamina")
	float CurrentStaminaCharacter;

    UFUNCTION(BlueprintCallable)
	float GetCurrentStamina();

    UFUNCTION(BlueprintCallable)
	void ChangeStamina(float amountOfStamina);

	


protected:
	
	virtual void BeginPlay() override;

public:	
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		
};
