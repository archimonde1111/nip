// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FlashlightActorComponent.generated.h"

class USpotLightComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NIP_API UFlashlightActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFlashlightActorComponent();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Flashlight")
	USpotLightComponent* Flashlight;

	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ToggleFlashlight();

	UFUNCTION(BlueprintCallable)
	float GetCurrentBattery();

	void SetFlashlightIntensity();

   

protected:
  
     UPROPERTY(EditAnywhere, Category = "Intensity")
	float FlashlightIntensity = 10000.f;
	
    void ChangeBattery(float batteryChangeValue);

	void ChangeBatteryMinusPlus(float DeltaTime);
    
	UPROPERTY(EditAnywhere, Category = "Flashlight Status")
	bool flashlightStatus;

    UPROPERTY(EditAnywhere, Category = "Battery")
	float BatteryCurrentValue;

    UPROPERTY(EditAnywhere, Category = "Battery")
	bool BatteryStatus;
  
};
