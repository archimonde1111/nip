// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Action_Base.generated.h"

class AActor;

/**
 * This is base class for your actions, you can think of it as abstract class
 * When overriding in BP remember that if you want to call c++ implementation you need to call PARENT function
 */
UCLASS(Blueprintable, EditInlineNew)
class NIP_API UAction_Base : public UObject
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Action_Base")
		AActor* Owner;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Action_Base")
		AActor* Interactor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Action_Base")
		bool bShouldTick = false;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Action_Base")
		bool bActionStarted = false;

protected:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Action_Base")
	void BeginAction();
	virtual void BeginAction_Implementation();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Action_Base")
	void EndAction();
	virtual void EndAction_Implementation();

public:
	UFUNCTION(BlueprintCallable, Category = "Action_Base")
	virtual void SetOwner(AActor* inOwner) { Owner = inOwner; }

	UFUNCTION(BlueprintCallable, Category = "Action_Base")
	virtual void Interact(AActor* inInteractor);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Action_Base")
	void TickAction();
	virtual void TickAction_Implementation();


	UFUNCTION(BlueprintCallable, Category = "Action_Base")
		bool CanActionTick() { return (bShouldTick && bActionStarted); }
};
