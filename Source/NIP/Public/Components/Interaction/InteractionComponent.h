// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InteractionComponent.generated.h"


class UAction_Base;
class AControllableCharacter;


/**
 * InteractionComponent allows other actors with InteractionManagerComponent to interact with owning actor
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NIP_API UInteractionComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	void SetupCustomDepthRenderingInOwnersStaticMeshes();

	void SpawnActionObject();

	bool CanInteract(AActor* otherActor);

protected:
	/**
	 * If this value is empty none will be able to interact
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "InteractionComponent | Settings")
		TSubclassOf<AControllableCharacter> ClassThatCanInteract = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Instanced, Category = "InteractionComponent | Settings")
		UAction_Base* AvailableAction = nullptr;

protected:
	virtual void BeginPlay() override;

public:	
	UInteractionComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Interact(AActor* interactor);
	void HighlightInteractable(bool bShouldHighlight);

};
