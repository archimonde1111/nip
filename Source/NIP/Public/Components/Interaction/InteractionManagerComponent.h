// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InteractionManagerComponent.generated.h"

class UInteractionComponent;

/**
 * bUseMultiLineTrace with this set to true interaction have 'queue' it means that player can interact with every interactable in range of MultiLneTrace
 * 		NOTE: after interaction interactable is moved to the end of queue
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NIP_API UInteractionManagerComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	void HandleLineTraceUsage();

	void SendMultiLineTrace(TArray<FHitResult>& outHitResult);
	bool GetCameraWorldLocationAndDirection(FVector& cameraWorldLocation, FVector& cameraWorldDirection);
	FVector2D GetViewportCrosshairPosition(APlayerController* playerController);

	void HighlightInteractable(UInteractionComponent* interactionComponentToHighlight, bool bHighlightInteractable);

	void ClearArrays();
	//Checks if Actors from HitResult have InteractionComponent
	void SortHitResults(const TArray<FHitResult>& hitResults);

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Properties", meta = (AllowPrivateAccess = "true"))
		TArray<UInteractionComponent*> AvailableInteractions = {};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings | LineTrace", meta = (AllowPrivateAccess = "true", EditCondition = "bUseMultiLineTrace"))
		float CrosshairPositionX = 0.5f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Settings | LineTrace", meta = (AllowPrivateAccess = "true", EditCondition = "bUseMultiLineTrace"))
		float CrosshairPositionY = 0.5f;

protected:
	virtual void BeginPlay() override;

public:	
	UInteractionManagerComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "InteractionManger")
	void PerformInteraction(AActor* interactor);

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Base")
		float ActorInteractionRange = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Base")
		bool bUseMultiLineTrace = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Base")
		bool bHighlighFirstInteractable = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings | Debug")
		bool bDebugMultiLineTrace = false;
};
