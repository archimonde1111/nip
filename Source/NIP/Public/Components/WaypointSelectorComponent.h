// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "WaypointSelectorComponent.generated.h"


class AWaypointContainer;

UENUM(BlueprintType)
enum class ERouteStatus : uint8
{
  ESC_Random     UMETA(DisplayName = "Random"),
  ESC_InSequence     UMETA(DisplayName = "InSequence"),
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class NIP_API UWaypointSelectorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UWaypointSelectorComponent();

	void SelectWaypoint();

	void SetWaypoint();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 RouteNumber= 0;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector SelectedWaypoint;

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    ERouteStatus RouteStatus;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ArrayIterator = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 RandomNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bActorWaiting = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeWait = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 RouteChange = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = WaypointContainer)
	TWeakObjectPtr<AWaypointContainer> WaypointContainer;


public:	

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
