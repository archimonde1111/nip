// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DayNightSystem.generated.h"

class ADirectionalLight;
class AActor;

UCLASS()
class NIP_API ADayNightSystem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADayNightSystem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
protected:

	UPROPERTY(EditAnywhere, Category = "Sky")
	AActor* Sun;

	UPROPERTY(EditAnywhere, Category = "Sky")
	ADirectionalLight* LightSource;

	UPROPERTY(EditAnywhere, Category = "Sky")
	float TurnRate;

	UPROPERTY(EditAnywhere, Category = "Sky")
	bool EnableCycle = false;

	void LightSourceRotator(float DeltaTime);

};
