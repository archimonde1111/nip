// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "NIPInGameHUD.generated.h"

class UUserWidget;
class UUserWidgetClass;

UCLASS()
class NIP_API ANIPInGameHUD : public AHUD
{
	GENERATED_BODY()
	
public:
   
   UPROPERTY(EditDefaultsOnly, Category = "Widget")
   TSubclassOf<UUserWidget> Widget;

   UUserWidgetClass* MyWidget;

protected:
  virtual void BeginPlay() override;
};
