// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UserWidgetClass.generated.h"

/**
 * 
 */
UCLASS()
class NIP_API UUserWidgetClass : public UUserWidget
{
	GENERATED_BODY()
	
};
