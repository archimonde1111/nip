// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Pawns/Characters/ControllableCharacter.h"
#include "AICharacter.generated.h"

class UActorRoute;

UCLASS()
class NIP_API AAICharacter : public AControllableCharacter
{
	GENERATED_BODY()
	
public: 

   AAICharacter();

   virtual void BeginPlay() override;
};
