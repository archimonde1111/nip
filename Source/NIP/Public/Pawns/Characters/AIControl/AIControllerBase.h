// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AIControllerBase.generated.h"

class UWaypointSelectorComponent;

UCLASS()
class NIP_API AAIControllerBase : public AAIController
{
	GENERATED_BODY()

public:
   
   AAIControllerBase();

   virtual void BeginPlay() override;

   virtual void Tick(float DeltaSeconds) override;

   virtual void OnPossess(APawn* Pawn) override;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	UWaypointSelectorComponent* WaypointSelectorComponent;
	
protected:

   virtual FRotator GetControlRotation() const override;

   virtual void MoveAIToLocation();

   


};
