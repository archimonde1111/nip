// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Pawns/Characters/AIControl/AIControllerBase.h"
#include "AIEnemyControllerClass.generated.h"

class AControllableCharacter;
class UAISenseConfig_Sight;

UCLASS()
class NIP_API AAIEnemyControllerClass : public AAIControllerBase
{
	GENERATED_BODY()

public:
   
   AAIEnemyControllerClass();

protected:

    virtual void BeginPlay() override;

    virtual void Tick(float DeltaSeconds) override;

	virtual void MoveAIToLocation() override;

	void SetInConstructor();

    UFUNCTION()
	void OnPawnDetected(const TArray<AActor*> &DetectedPawns);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float AISightRadius = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float AISightAge = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float AILoseSightRadius = AISightRadius + 50.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float AIFieldOfView = 90.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	UAISenseConfig_Sight* SightConfig;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	bool bIsPlayerDetected = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	float DistanceToPlayer = 0.0f;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	bool PawnDetectBool = false;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	AControllableCharacter* PlayerCharacter;
	
};
