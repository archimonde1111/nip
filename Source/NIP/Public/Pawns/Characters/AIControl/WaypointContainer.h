// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/Map.h"
#include "WaypointContainer.generated.h"


//TODO: ZASTANOWIĆ SIE NAD PRZERZUTEM TEGO NA UOBJECT

USTRUCT(BlueprintType)
struct FLocationAndWaitTime
{
   GENERATED_BODY();

   UPROPERTY(EditAnywhere, BlueprintReadWrite)
   float WaitTime;

   UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MakeEditWidget = "true"))
   FVector TargetWaypoints;
 
};

USTRUCT(BlueprintType)
struct FAIRouteWaypoints
{
   GENERATED_BODY();

   UPROPERTY(EditAnywhere, BlueprintReadWrite)
   TArray<FLocationAndWaitTime> LocationAndTime;

};

UCLASS()
class NIP_API AWaypointContainer : public AActor
{
	GENERATED_BODY()
	
public:	

	AWaypointContainer();

	UFUNCTION()
	FLocationAndWaitTime GenerateWaypoint(int32 RouteIndex, int32 WaypointIndex);

	UFUNCTION()
	TArray<FLocationAndWaitTime> GetLocationAndWaitTimeArray(int32 RouteIndex);
	


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FAIRouteWaypoints> RouteSelection;

protected:

	virtual void BeginPlay() override;


public:	

	virtual void Tick(float DeltaTime) override;

};
