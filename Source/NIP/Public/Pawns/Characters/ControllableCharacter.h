// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ControllableCharacter.generated.h"

class UInputComponent;
class UInteractionManagerComponent;

UENUM(BlueprintType)
enum class EStatusCharacter : uint8
{
  ESC_Sprint     UMETA(DisplayName = "Sprint"),
  ESC_Walk      UMETA(DisplayName = "Walk"),
  ESC_Crouch     UMETA(DisplayName = "Crouch")
};

UCLASS()
class NIP_API AControllableCharacter : public ACharacter
{
	GENERATED_BODY()

private:
	void CreateInteractionManager();


	

protected:
	virtual void BeginPlay() override;

	virtual void BindMovementInputs(UInputComponent& PlayerInputComponent);
	virtual void CrouchStart();
	virtual void CrouchStop();
	virtual void Sprint();
	virtual void SprintStop();
	virtual void MoveForward(float value);
	virtual void MoveRight(float value);
	
	virtual void TurnAtRate(float value);
	virtual void LookUpAtRate(float value);

	virtual void Interact();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    EStatusCharacter StatusCharacter;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float baseTurnRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
	float baseLookUpAtRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Character Movement || Walking")
	float SpeedMultiplayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CharacterComponents")
		UInteractionManagerComponent* InteractionManager = nullptr;

public:	
	AControllableCharacter();
	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
