// Miki&Dawid

#pragma once

#include "CoreMinimal.h"
#include "Pawns/Characters/ControllableCharacter.h"
#include "PlayerCharacter.generated.h"


class USpringArmComponent;
class UCameraComponent;
class UInputComponent;
class UCharacterAbilities;
class UMatineeCameraShake;
class USpotLightComponent;
class UFlashlightActorComponent;




UCLASS()
class NIP_API APlayerCharacter : public AControllableCharacter
{
	GENERATED_BODY()

public:

    APlayerCharacter();

	virtual void BeginPlay() override;

	
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera")
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere)
	UFlashlightActorComponent* FlashlightInCharacter;
	
	void Sprint();
	void SprintStop();
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void SprintTestStamina(float DeltaTime);
	void CameraShakeWalk(float scale);
	void CameraShakeSprint(float scale);
	void MoveForwardTestMethod();
	void FlashlightSystem();

	UPROPERTY()
	UCharacterAbilities* CharacterAbilitiesPointer;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UMatineeCameraShake> CamShakeWalk;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UMatineeCameraShake> CamShakeSprint;


	virtual void Tick(float DeltaTime) override;

	

public:


};
